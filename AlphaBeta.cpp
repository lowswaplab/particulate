
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2020-2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"

using namespace LowSWaPLab;

AlphaBeta::AlphaBeta()
  {
  reset();
  }

void AlphaBeta::reset()
  {
  dt = 0.5;
  xk_1 = 0;
  vk_1 = 0;
  a = 0.85;
  b = 0.005;
  xk = 0;
  vk = 0;
  rk = 0;
  }

void AlphaBeta::setup(const double _alpha, const double _beta,
                      const double _dt)
  {
  a = _alpha;
  b = _beta;
  dt = _dt;
  }

// code from
// https://en.wikipedia.org/wiki/Alpha_beta_filter
double AlphaBeta::update(const double xm)
  {
  xk = xk_1 + ( vk_1 * dt );
  vk = vk_1;

  rk = xm - xk;

  xk += a * rk;
  vk += ( b * rk ) / dt;

  xk_1 = xk;
  vk_1 = vk;

  return(xk_1);
  }

double AlphaBeta::current()
  {
  return(xk_1);
  }

