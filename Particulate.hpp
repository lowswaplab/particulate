
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2020-2021 Low SWaP Lab lowswaplab.com

#ifndef PARTICULATE_HPP
  #define PARTICULATE_HPP

  #include <stdint.h>
  #include <math.h>
  #include <cstddef>

  /*! \brief Low SWaP Lab namespace
   *
   *  Low SWaP Lab software can be found at lowswaplab.com
   */
  namespace LowSWaPLab
    {

    //! αβ Filter
    //! https://en.wikipedia.org/wiki/Alpha_beta_filter
    class AlphaBeta
      {
      public:
        //! Object constructor.
        AlphaBeta();
        void reset();
        void setup(const double alpha, const double beta, const double dt);
        double update(const double value);
        double current();

      private:
        double dt;
        double xk_1;
        double vk_1;
        double a;
        double b;
        double xk;
        double vk;
        double rk;
      };

    //! Super simple circular buffer for use by, for example, LLS
    class Circular
      {
      public:
        //! Object constructor.
        //! Call it like this:
        //!   #define VAL_SIZE (10)
        //!   double vals[VAL_SIZE];
        //!   LowSWaPLab::Circular ring(vals, VAL_SIZE);
        Circular(double buf[], const size_t size);
        void reset();
        void push(const double val);
      private:
        double *buf;
        size_t size;
        size_t index;
      };

    //! Cumulative Moving Average Filter
    //! https://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average
    class CMA
      {
      public:
        //! Object constructor.
        CMA();
        void reset();
        double update(const double value);
        double current();

      private:
        double cma;
        uint32_t n;
      };

    //! Exponential Moving Average Filter
    //! https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
    class EMA
      {
      public:
        //! Object constructor.
        EMA();
        void reset();
        void setup(const double alpha);
        double update(const double value);
        double current();

      private:
        double a;
        double ema;
        double weightedsum;
        double weightedcount;
      };

    // linear least squares fit
    // return slope of the line and, optionally, y intercept
    float LLS(const float x[], const float y[], const int size,
              float *yint=NULL);
    }

#endif

