
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2020-2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"

using namespace LowSWaPLab;

CMA::CMA()
  {
  reset();
  }

void CMA::reset()
  {
  cma = 0;
  n = 0;
  }

// code from
// https://en.wikipedia.org/wiki/Moving_average#Cumulative_moving_average
double CMA::update(const double x)
  {
  n += 1;
  cma = cma + ((x - cma) / n);

  return(cma);
  }

double CMA::current()
  {
  return(cma);
  }

