
# Particulate

Particulate is a set of lightweight filtering, averaging, and related
algorithms for IoT devices.

Particulate is specifically design for use in low size, weight, and power
(SWaP) systems.



# Platforms

Particulate has been ported to the Arduino and Linux development
environments.



# Source Code

[Particulate Git Repository](https://gitlab.com/lowswaplab/particulate).



# Contribution

If you find this software useful, please consider financial support
for future development via
[PayPal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GY9C5FN54TCKL&source=url).



# Author

[Low SWaP Lab](https://www.lowswaplab.com/)



# Copyright

Copyright © 2020-2021 Low SWaP Lab [lowswaplab.com](https://www.lowswaplab.com/)

