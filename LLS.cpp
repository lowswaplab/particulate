
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"

// https://forum.arduino.cc/index.php?topic=96292.0
float LowSWaPLab::LLS(const float x[], const float y[], const int n, float *b)
  {
  int index;
  float sum_x=0;
  float sum_y=0;
  float sum_x2=0;
  float sum_y2=0;
  float sum_xy=0;
  float m;

  for (index=0; index < n; ++index)
    {
    sum_x += x[index];
    sum_y += y[index];
    sum_x2 += x[index] * x[index];
    sum_y2 += y[index] * y[index];
    sum_xy = sum_xy + x[index] * y[index];
    };

  m = (n * sum_xy - sum_x * sum_y) / (n * sum_x2 - sum_x * sum_x);
  if (b != NULL)
    *b = (sum_x2 * sum_y - sum_x * sum_xy) / (n * sum_x2 - sum_x * sum_x);

  return(m);
  }

