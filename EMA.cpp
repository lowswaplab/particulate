
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2020-2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"

using namespace LowSWaPLab;

EMA::EMA()
  {
  reset();
  }

void EMA::reset()
  {
  a = 0;
  ema = 0;
  weightedsum = 0;
  weightedcount = 0;
  }

void EMA::setup(const double _alpha)
  {
  // XXX error check must be between 0 and 1 inclusive
  a = _alpha;
  }

// code from
// https://en.wikipedia.org/wiki/Moving_average#Exponential_moving_average
double EMA::update(const double x)
  {
  weightedsum = x + (1 - a) * weightedsum;
  weightedcount = 1 + (1- a) * weightedcount;

  ema = weightedsum / weightedcount;

  return(ema);
  }

double EMA::current()
  {
  return(ema);
  }

