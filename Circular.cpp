
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"

using namespace LowSWaPLab;

Circular::Circular(double _buf[], const size_t _size)
  {
  buf = _buf;
  size = _size;
  reset();
  }

#include <stdio.h>

void Circular::reset()
  {
  size_t idx;

  index = 0;

  for (idx=0; idx < size; ++idx)
    buf[idx] = INFINITY;
  }

void Circular::push(const double val)
  {
  buf[index] = val;
  index++;
  if (index >= size)
    index = 0;
  }

