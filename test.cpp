
// Particulate
// https://gitlab.com/lowswaplab/particulate
// Copyright © 2021 Low SWaP Lab lowswaplab.com

#include "Particulate.hpp"
#include <stdio.h>

int main()
  {
  size_t index;
  #define VAL_SIZE (10)
  double vals[VAL_SIZE];

  LowSWaPLab::Circular ring(vals, VAL_SIZE);

  ring.push(0);
  ring.push(1);
  ring.push(2);
  ring.push(3);
  ring.push(4);
  ring.push(5);
  ring.push(6);
  ring.push(7);
  ring.push(8);
  ring.push(9);
  ring.push(10);
  ring.push(11);

  for (index=0; index < VAL_SIZE; ++index)
    if (!isinf(vals[index]))
      printf("%3lu: %f\n", index, vals[index]);

  }

